package com.twu.biblioteca;

import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class UserLibraryTest {

    @Test
    public void shouldReturnTrueIfCredentialsAreValid() {
        User user = new User("777-4445", "TW12", null, null, null, new ArrayList<Item>(10));
        UserCollection userCollection = new UserCollection(user);
        assertThat(userCollection.matches("777-4445", "TW12"), is(new User("777-4445", "TW12", null, null, null, new ArrayList<Item>(10))));
    }

    @Test
    public void shouldReturnFalseIfCredentialsAreInValid() {
        User user = new User("", "", "", "", "", new ArrayList<Item>(10)) {
            @Override
            public boolean matches(String libraryNumber, String password) {
                return false;
            }
        };
        UserCollection userCollection = new UserCollection(user);
        assertThat(userCollection.matches("777-4445", "TW124"), is(nullValue()));
    }

}

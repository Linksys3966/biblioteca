package com.twu.biblioteca;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class BookTest {
    @Test
    public void shouldPrintFormattedOutput() {
        String actual = new Book("Harry Potter", "Vivek", "1992", false).toString();
        assertThat(actual, is("Harry Potter                  Vivek                         1992                          \n"));
    }


    @Test
    public void shouldBeCheckOutAble() throws Exception {
        Book book = new Book("Harry Potter", "J.K. Rowling", "1997", false);
        assertThat(book.checkOut(), is(true));
        assertThat(book.checkOut(), is(false));
    }

    @Test
    public void shouldBeCheckInAble() throws Exception {
        Book book = new Book("Harry Potter", "J.K. Rowling", "1997", true);
        assertThat(book.checkIn(), is(true));
        assertThat(book.checkIn(), is(false));
    }

    @Test
    public void shouldBeNotAbleToCheckOutMoreThanOnce() throws Exception {
        Book book = new Book("Harry Potter", "J.K. Rowling", "1997", false);
        assertThat(book.checkOut(), is(true));
        assertThat(book.checkOut(), is(false));
    }
}

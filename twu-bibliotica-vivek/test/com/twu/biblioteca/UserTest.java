package com.twu.biblioteca;

import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class UserTest {

    @Test
    public void shouldReturnTrueIfUserMatches() throws Exception {
        User user = new User("777-4445", "TW12", "Vivek", "vivekpatil2092@gmail.com", "9096904102", new ArrayList<Item>(10));
        assertThat(user.matches("777-4445", "TW12"), is(true));
    }

    @Test
    public void shouldReturnFalseIfUserDoesNotMatch() throws Exception {
        User user = new User("777-4445", "TW12", "Vivek", "vivekpatil2092@gmail.com", "9096904102", new ArrayList<Item>(10));
        assertThat(user.matches("1234", "TW12"), is(false));
    }
}

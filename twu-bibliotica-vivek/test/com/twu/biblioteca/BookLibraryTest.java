package com.twu.biblioteca;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class BookLibraryTest {
    private Book[] allBooks = {
        new Book("Harry Potter", "Vivek", "1992", true),
        new Book("Four  States", "Dhanesh", "1991", true)
    };
    private BookLibrary books = new BookLibrary(allBooks);

    @Test
    public void shouldDisplaySpecificMessageIfThereIsNoBookInLibrary() throws Exception {
        BookLibrary bookCollection = new BookLibrary();
        String actual = bookCollection.toString();
        String expected = "No books In Library";
        assertThat(actual, is(expected));
    }

    @Test
    public void shouldDisplayNoBooksInLibraryIfAllBooksHaveBeenCheckedOut() throws Exception {
        books.checkOut("Harry Potter", null);
        books.checkOut("Four  States", null);
        String actual = books.toString();
        String expected = "No books In Library";
        assertThat(actual, is(expected));
    }

    @Test
    public void shouldOnlyDisplayBooksThatHaveNotBeenCheckedOut() throws Exception {
        Book[] allBooks = {
            new Book("Harry Potter", "Vivek", "1992", false),
            new Book("Four  States", "Dhanesh", "1991", true)
        };
        BookLibrary books = new BookLibrary(allBooks);
        String actual = books.toString().trim();
        assertThat(actual, is("Name                          Author                        Year                          \nHarry Potter                  Vivek                         1992"));
    }
}

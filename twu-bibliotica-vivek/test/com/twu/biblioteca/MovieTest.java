package com.twu.biblioteca;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MovieTest {
    @Test
    public void shouldPrintFormattedOutput() {
        String actual = new Movie("Holiday", "2014", "Yash Raj", "7", false).toString();
        assertThat(actual, is("Holiday                       Yash Raj                      2014                          \n"));
    }


    @Test
    public void shouldBeCheckOutAble() throws Exception {
        Movie movie = new Movie("Holiday", "2014", "Yash Raj", "7", false);
        assertThat(movie.checkOut(), is(true));
    }


    @Test
    public void shouldBeNotAbleToCheckOutMoreThanOnce() throws Exception {
        Movie movie = new Movie("Holiday", "2014", "Yash Raj", "7", false);
        assertThat(movie.checkOut(), is(true));
        assertThat(movie.checkOut(), is(false));
    }
}

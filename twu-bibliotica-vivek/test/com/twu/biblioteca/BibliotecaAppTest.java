package com.twu.biblioteca;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;

public class BibliotecaAppTest {

    private static final String WELCOME_MESSAGE = "Welcome to Biblioteca\n";
    private static final String MENU =
        "1.List Of Books\n" +
            "2.Check Out Book\n" +
            "3.Return Book\n" +
            "4.List Of Movies\n" +
            "5.Check Out A movie\n" +
            "6.Return a Movie\n" +
            "8.Quit\n";
    Movie[] movies = {
        new Movie("Holiday", "2014", "Subhash", "8", false),
        new Movie("Shehenshah", "1992", "Sajid", "9", false)
    };
    User[] users = {
        new User("777-4445", "TW12", "Vivek", "vivekpatil2092@gmail.com", "9096904102", new ArrayList<Item>(10)),
        new User("654-8866", "Apple", "Chandan", "chandan00104@gmail.com", "9420473943", new ArrayList<Item>(10))
    };
    UserCollection userCollection = new UserCollection(users);
    User currentUser = null;
    private ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    private PrintStream printStream = new PrintStream(byteArrayOutputStream);
    Printer printer = new Printer(printStream);


    private Book[] allBooks = {
        new Book("Harry Potter", "Vivek", "1992", false),
        new Book("Four  States", "Dhanesh", "1991", false)
    };

    BookLibrary bookCollection = new BookLibrary(allBooks);
    MovieLibrary movieCollection = new MovieLibrary(movies);
    LibraryManager libraryManager = new LibraryManager(bookCollection, movieCollection, userCollection);

    @Test
    public void shouldWelcomeUserAndShowMenu() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("".getBytes());
        new BibliotecaApp(printer, inputStream).run(libraryManager, currentUser);
        String actual = byteArrayOutputStream.toString();
        assertThat(actual, is(WELCOME_MESSAGE + MENU));
    }

    @Test
    public void shouldShowInvalidOptionMessageForInvalidInputAndShowMenuAgain() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("9".getBytes());
        new BibliotecaApp(printer, inputStream).run(libraryManager, currentUser);
        String actual = byteArrayOutputStream.toString();
        assertThat(actual, is(WELCOME_MESSAGE + MENU + "Select a Valid Option" + MENU));
    }


    @Test
    public void shouldDisplayTheListOfBooksIfOptionOneIsSelected() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("1".getBytes());
        BookLibrary bookCollection = new BookLibrary(allBooks);
        new BibliotecaApp(printer, inputStream).run(libraryManager, currentUser);
        String actual = byteArrayOutputStream.toString();
        String booksList = bookCollection.toString();
        assertThat(actual, is(WELCOME_MESSAGE + MENU + booksList  + MENU));
    }

    @Test
    public void shouldShowSuccessMessageOnSuccessfulBookCheckout() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("2\n777-4445\nTW12\nHarry Potter\n".getBytes());
        BookLibrary bookCollection = new BookLibrary() {
            @Override
            public boolean checkOut(String name, User user) {
                return true;
            }
        };
        new BibliotecaApp(printer, inputStream).run(libraryManager, currentUser);
        String actual = byteArrayOutputStream.toString().trim();
        assertThat(actual, containsString("Thank You..Enjoy The Book"));
    }

    @Test
    public void shouldShowFailureMessageOnInvalidLoginDuringCheckOut() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("2\n777-4447\nTW12\nHarry Potter\n".getBytes());
        BookLibrary bookCollection = new BookLibrary() {
            @Override
            public boolean checkOut(String name, User user) {
                return true;
            }
        };
        new BibliotecaApp(printer, inputStream).run(libraryManager, currentUser);
        String actual = byteArrayOutputStream.toString().trim();
        assertThat(actual, containsString("Invalid Login"));

    }

    @Test
    public void shouldShowFailureMessageOnUnsuccessfulBookCheckout() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("2\n777-4445\nTW12\nThree Mistakes\n".getBytes());
        BookLibrary bookCollection = new BookLibrary() {
            @Override
            public boolean checkOut(String name, User user) {
                return false;
            }
        };
        new BibliotecaApp(printer, inputStream).run(libraryManager, currentUser);
        String actual = byteArrayOutputStream.toString().trim();
        assertThat(actual, containsString("Sorry the Book is not Available"));
    }

    @Test
    public void shouldShowSuccessMessageOnSuccessfulBookReturn() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("3\n777-4445\nTW12\nFour  States\n".getBytes());
        BookLibrary bookCollection = new BookLibrary() {
            @Override
            public boolean checkIn(String name, User user) {
                return true;
            }
        };
        new BibliotecaApp(printer, inputStream).run(libraryManager, currentUser);
        String actual = byteArrayOutputStream.toString().trim();
        assertThat(actual, containsString("Thank You For Returning The Book"));
    }

    @Test
    public void shouldShowFailureMessageOnUnsuccessfulBookReturn() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("3\n777-4445\nTW12\nAngels\n".getBytes());
        BookLibrary bookCollection = new BookLibrary() {
            @Override
            public boolean checkIn(String name, User user) {
                return false;
            }
        };
        new BibliotecaApp(printer, inputStream).run(libraryManager, currentUser);
        String actual = byteArrayOutputStream.toString().trim();
        assertThat(actual, containsString("Sorry You are returning the wrong book"));
    }

    @Test
    public void shouldShowSuccessMessageOnSuccessfulMovieCheckout() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("5\n777-4445\nTW12\nHoliday\n".getBytes());
        MovieLibrary movieCollection = new MovieLibrary() {
            @Override
            public boolean checkOut(String name, User user) {
                return true;
            }
        };
        libraryManager.movieCollection = movieCollection;
        new BibliotecaApp(printer, inputStream).run(libraryManager, currentUser);
        String actual = byteArrayOutputStream.toString().trim();
        assertThat(actual, containsString("Thank You..Enjoy The Movie"));
    }

    @Test
    public void shouldShowFailureMessageOnUnsuccessfulMovieCheckout() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("5\n777-4445\nTW12\nHoliday\n".getBytes());
        MovieLibrary movieCollection = new MovieLibrary() {
            @Override
            public boolean checkOut(String name, User user) {
                return false;
            }
        };
        libraryManager.movieCollection = movieCollection;
        new BibliotecaApp(printer, inputStream).run(libraryManager, currentUser);
        String actual = byteArrayOutputStream.toString().trim();
        assertThat(actual, containsString("Sorry the Movie is not Available"));
    }

    @Test
    public void shouldShowSuccessMessageOnSuccessfulMovieReturn() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("6\n777-4445\nTW12\nHoliday\n".getBytes());
        MovieLibrary movieCollection = new MovieLibrary() {
            @Override
            public boolean checkIn(String name, User user) {
                return true;
            }
        };
        libraryManager.movieCollection = movieCollection;
        new BibliotecaApp(printer, inputStream).run(libraryManager, currentUser);
        String actual = byteArrayOutputStream.toString().trim();
        assertThat(actual, containsString("Thank You For Returning The Movie"));
    }

    @Test
    public void shouldShowFailureMessageOnUnsuccessfulMovieReturn() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("6\n777-4445\nTW12\nHoliday\n".getBytes());
        MovieLibrary movieCollection = new MovieLibrary() {
            @Override
            public boolean checkIn(String name, User user) {
                return false;
            }
        };
        libraryManager.movieCollection = movieCollection;
        new BibliotecaApp(printer, inputStream).run(libraryManager, currentUser);
        String actual = byteArrayOutputStream.toString().trim();
        assertThat(actual, containsString("Sorry You are returning the wrong Movie"));
    }

}

package com.twu.biblioteca;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
* Created by vivekpatil on 6/23/14.
*/

public class LibraryTest {

    @Test
    public void shouldReturnTrueifItemIsSuccessfullyCheckedOut() throws Exception {
        Item item = new Item("Harry Potter", false) {
            @Override
            public boolean checkOut() {
                return true;
            }

            @Override
            public String toString() {
                return null;
            }

            @Override
            public String[] getFields() {
                return new String[0];
            }
        };
        Library library = new Library(item) {
            @Override
            public String toString() {
                return null;
            }

            @Override
            protected String getItemName() {
                return null;
            }

            @Override
            public String getTitles() {
                return null;
            }
        };
        assertThat(library.checkOut("Harry Potter", null), is(true));
    }

    @Test
    public void shouldReturnFalseifWeAreUnableToCheckOutAItem() throws Exception {
        Item item = new Item("Harry Potter", false) {
            @Override
            public boolean checkOut() {
                return false;
            }

            @Override
            public String toString() {
                return null;
            }

            @Override
            public String[] getFields() {
                return new String[0];
            }
        };
        Library library = new Library(item) {
            @Override
            public String toString() {
                return null;
            }

            @Override
            protected String getItemName() {
                return null;
            }

            @Override
            public String getTitles() {
                return null;
            }
        };
        assertThat(library.checkOut("Harry Potter", null), is(false));


    }

    @Test
    public void shouldReturnFalseIfAnAttemptIsMadeToCheckoutAUnavailableItem() throws Exception {
        Item item = new Item("Harry Potter", false) {
            @Override
            public String toString() {
                return null;
            }

            @Override
            public String[] getFields() {
                return new String[0];
            }
        };
        Library library = new Library(item) {
            @Override
            public String toString() {
                return null;
            }

            @Override
            protected String getItemName() {
                return null;
            }

            @Override
            public String getTitles() {
                return null;
            }
        };
        assertThat(library.checkOut("dfhbdhfb", null), is(false));

    }

    @Test
    public void shouldReturnTrueifItemIsSuccessfullyCheckedIn() throws Exception {
        Item item = new Item("Harry Potter", false) {

            @Override
            public boolean checkIn() {
                return true;
            }

            @Override
            public String toString() {
                return null;
            }

            @Override
            public String[] getFields() {
                return new String[0];
            }
        };
        Library library = new Library(item) {
            @Override
            public String toString() {
                return null;
            }

            @Override
            protected String getItemName() {
                return null;
            }

            @Override
            public String getTitles() {
                return null;
            }
        };
        assertThat(library.checkIn("Harry Potter", null), is(true));
    }

    @Test
    public void shouldReturnFalseifItemCannotbeCheckedIn() throws Exception {
        Item item = new Item("Harry Potter", false) {

            @Override
            public boolean checkIn() {
                return false;
            }

            @Override
            public String toString() {
                return null;
            }

            @Override
            public String[] getFields() {
                return new String[0];
            }
        };
        Library library = new Library(item) {
            @Override
            public String toString() {
                return null;
            }

            @Override
            protected String getItemName() {
                return null;
            }

            @Override
            public String getTitles() {
                return null;
            }
        };
        assertThat(library.checkIn("Harry Potter", null), is(false));
    }

    @Test
    public void shouldReturnFalseIfAnAttemptIsMadeToCheckInAUnavailableItem() throws Exception {
        Item item = new Item("Harry Potter", true) {
            @Override
            public String toString() {
                return null;
            }

            @Override
            public String[] getFields() {
                return new String[0];
            }
        };
        Library library = new Library(item) {
            @Override
            public String toString() {
                return null;
            }

            @Override
            protected String getItemName() {
                return null;
            }

            @Override
            public String getTitles() {
                return null;
            }
        };
        assertThat(library.checkIn("sbcb", null), is(false));
    }

}

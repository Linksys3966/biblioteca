package com.twu.biblioteca;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MovieLibraryTest {
    private Movie[] allMovies = {
        new Movie("Holiday", "2014", "Subhash", "8", false),
        new Movie("Shehenshah", "1992", "Sajid", "9", false)
    };
    private MovieLibrary movies = new MovieLibrary(allMovies);

    @Test
    public void shouldDisplaySpecificMessageIfThereIsNoMovieInLibrary() throws Exception {
        MovieLibrary movieCollection = new MovieLibrary();
        String actual = movieCollection.toString();
        String expected = "No movies In Library";
        assertThat(actual, is(expected));
    }

    @Test
    public void shouldDisplayNoMoviesInLibraryIfAllMoviesHaveBeenCheckedOut() throws Exception {
        movies.checkOut("Holiday", null);
        movies.checkOut("Shehenshah", null);
        String actual = movies.toString();
        String expected = "No movies In Library";
        assertThat(actual, is(expected));
    }

    @Test
    public void shouldOnlyDisplayMoviesThatHaveNotBeenCheckedOut() throws Exception {
        Movie[] allMovies = {
            new Movie("Holiday", "2014", "Subhash", "8", false),
            new Movie("Shehenshah", "1992", "Sajid", "9", true)
        };
        MovieLibrary movieCollection = new MovieLibrary(allMovies);
        String actual = movieCollection.toString().trim();
        assertThat(actual, is("NAME                          DIRECTOR                      YEAR                          \nHoliday                       Subhash                       2014"));
    }


}

package com.twu.biblioteca;

public abstract class Item {
    private static final String FORMAT = "%-30s%-30s%-30s\n";
    String name;
    boolean checkedOut;

    Item(String name, boolean checkedOut) {
        this.name = name;
        this.checkedOut = checkedOut;
    }

    public boolean available() {
        return checkedOut;
    }

    public boolean checkOut() {
        if (!available()) {
            checkedOut = true;
            return true;
        }
        return !checkedOut;
    }

    public boolean checkIn() {
        if (available()) {
            checkedOut = false;
            return true;
        }
        return checkedOut;
    }

    public String toString() {
        return String.format(String.format(FORMAT, getFields()));
    }

    public abstract String[] getFields();
}


package com.twu.biblioteca;

import java.util.Arrays;
import java.util.List;

public abstract class Library {
    List<Item> items;

    Library(Item... items) {
        this.items = Arrays.asList(items);

    }

    public boolean checkOut(String name, User user) {
        for (Item item : items) {
            if (item.name.equals(name)) {
                user.items.add(item);
                return item.checkOut();
            }
        }
        return false;
    }

    public boolean checkIn(String name, User user) {
        for (Item item : items) {
            if (item.name.equals(name)) {
                user.items.remove(item);
                return (item.checkIn());
            }
        }
        return false;
    }

    public String toString() {
        StringBuilder ret = new StringBuilder();
        for (Item item : items) {
            if (!item.available()) {
                ret.append(item);
            }
        }
        if (ret.length() == 0) {
            ret.append("No ").append(getItemName()).append(" In Library");
            return ret.toString();
        }
        return getTitles() + ret.toString();
    }

    protected abstract String getItemName();

    protected abstract String getTitles();
}

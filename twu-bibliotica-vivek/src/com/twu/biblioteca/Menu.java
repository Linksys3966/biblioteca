package com.twu.biblioteca;

import java.util.Scanner;

enum Menu {

    LISTBOOKS("List Of Books", "1") {
        @Override
        public ViewState execute(Scanner scanner, User currentUser, LibraryManager libraryManager, Printer printer) {
            printer.displayOutput(libraryManager.bookLibrary.toString());
            return new ViewState(currentUser, true);
        }
    },
    CHECKOUT("Check Out Book", "2") {
        @Override
        public ViewState execute(Scanner scanner, User currentUser, LibraryManager libraryManager, Printer printer) {
            currentUser = libraryManager.login(scanner, printer, libraryManager.userCollection, currentUser);
            if (userDoesNotExists(currentUser)) return new ViewState(currentUser, true);
            return libraryManager.checkOutBook(scanner, printer, currentUser);
        }
    },

    RETURNBOOK("Return Book", "3") {
        @Override
        public ViewState execute(Scanner scanner, User currentUser, LibraryManager libraryManager, Printer printer) {
            currentUser = libraryManager.login(scanner, printer, libraryManager.userCollection, currentUser);
            if (userDoesNotExists(currentUser)) return new ViewState(currentUser, true);
            return libraryManager.returnBook(scanner, printer, currentUser);
        }
    },

    LISTOFMOVIES("List Of Movies", "4") {
        @Override
        public ViewState execute(Scanner scanner, User currentUser, LibraryManager libraryManager, Printer printer) {
            printer.displayOutput(libraryManager.movieCollection.toString());
            return new ViewState(currentUser, true);
        }
    },

    CHECKOUTMOVIE("Check Out A movie", "5") {
        @Override
        public ViewState execute(Scanner scanner, User currentUser, LibraryManager libraryManager, Printer printer) {
            currentUser = libraryManager.login(scanner, printer, libraryManager.userCollection, currentUser);
            if (userDoesNotExists(currentUser)) return new ViewState(currentUser, true);
            return libraryManager.checkOutMovie(scanner, printer, currentUser);

        }
    },

    RETURNMOVIE("Return a Movie", "6") {
        @Override
        public ViewState execute(Scanner scanner, User currentUser, LibraryManager libraryManager, Printer printer) {
            currentUser = libraryManager.login(scanner, printer, libraryManager.userCollection, currentUser);
            if (userDoesNotExists(currentUser)) return new ViewState(currentUser, true);
            return libraryManager.returnMovie(scanner, printer, currentUser);
        }
    },

    INFORMATION("User Information", "7") {
        @Override
        public ViewState execute(Scanner scanner, User currentUser, LibraryManager libraryManager, Printer printer) {
            currentUser = libraryManager.login(scanner, printer, libraryManager.userCollection, currentUser);
            if (userDoesNotExists(currentUser)) return new ViewState(currentUser, true);
            printer.displayOutput(currentUser.toString());
            return new ViewState(currentUser, true);
        }

    },
    LOGOUT("Sign Out", "8") {
        @Override
        public ViewState execute(Scanner scanner, User currentUser, LibraryManager libraryManager, Printer printer) {
            currentUser = null;
            return new ViewState(currentUser, true);
        }

    },
    INVALIDOPTION("", "10") {
        @Override
        public ViewState execute(Scanner scanner, User currentUser, LibraryManager libraryManager, Printer printer) {
            printer.displayOutput("Select a Valid Option");
            return new ViewState(currentUser, true);
        }

        @Override
        public String toString() {
            return "";
        }
    },
    QUIT("Quit", "9") {
        @Override
        public ViewState execute(Scanner scanner, User currentUser, LibraryManager libraryManager, Printer printer) {
            return new ViewState(currentUser, false);
        }
    };


    public static boolean userDoesNotExists(User currentUser) {
        if (currentUser == null) {
            return true;
        }
        return false;
    }

    String option;
    String number;

    Menu(String option, String number) {
        this.option = option;
        this.number = number;
    }

    public static Menu find(String id) {
        for (Menu menu : Menu.values()) {
            if (menu.matches(id))
                return menu;
        }
        return Menu.INVALIDOPTION;
    }

    public static void printMenu(Printer printer, User currentUser) {
        for (Menu menu : Menu.values()) {
            if (!menu.equals(Menu.INFORMATION)) {
                printer.displayOutput(menu.toString());
            } else {
                if (currentUser != null)
                    printer.displayOutput(menu.toString());
            }
        }
    }


    private boolean matches(String otherNumber) {
        return this.number.equals(otherNumber);
    }

    @Override
    public String toString() {
        return String.format("%s.%s\n", number, option);
    }

    public abstract ViewState execute(Scanner scanner, User currentUser, LibraryManager libraryManager, Printer printer);
}

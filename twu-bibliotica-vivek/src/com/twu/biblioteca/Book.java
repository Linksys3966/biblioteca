package com.twu.biblioteca;

public class Book extends Item {
    public String name;
    public String author;
    public String year;
    public boolean checkedOut;


    public Book(String name, String author, String year, boolean checkedOut) {
        super(name, checkedOut);
        this.name = name;
        this.author = author;
        this.year = year;
        this.checkedOut = checkedOut;
    }

    public String[] getFields() {
        return new String[]{name, author, year};
    }
}

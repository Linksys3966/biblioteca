package com.twu.biblioteca;

import java.util.List;

public class User {

    String libraryNumber;
    String password;
    String username;
    String emailAddress;
    String phoneNumber;
    List<Item> items;

    User(String libraryNumber, String password, String username, String emailAddress, String phoneNumber, List<Item> items) {
        this.libraryNumber = libraryNumber;
        this.password = password;
        this.username = username;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
        this.items = items;
    }

    public boolean matches(String libraryNumber, String password) {
        return this.libraryNumber.equals(libraryNumber) && this.password.equals(password);
    }

    public String toString() {
        return username + " " + emailAddress + " " + phoneNumber + " " + items.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return libraryNumber.equals(user.libraryNumber) && password.equals(user.password);
    }

    @Override
    public int hashCode() {
        int result = libraryNumber.hashCode();
        result = 31 * result + password.hashCode();
        return result;
    }
}

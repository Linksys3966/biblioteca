package com.twu.biblioteca;

import java.io.PrintStream;

public class Printer {

    private PrintStream printStream;
    public Printer(PrintStream printStream)
    {
        this.printStream = printStream;
    }

    public void displayOutput(String output)
    {
        printStream.print(output);
    }
}

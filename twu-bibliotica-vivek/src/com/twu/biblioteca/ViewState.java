package com.twu.biblioteca;

public class ViewState {
    User currentUser;
    boolean continueRunning;

    ViewState(User currentUser, boolean continueRunning) {
        this.currentUser = currentUser;
        this.continueRunning = continueRunning;
    }
}

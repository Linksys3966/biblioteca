package com.twu.biblioteca;

import java.util.Arrays;
import java.util.List;

public class BookLibrary extends Library {
    List<Book> books;

    BookLibrary(Book... books) {
        super(books);
        this.books = Arrays.asList(books);
    }

    @Override
    protected String getItemName() {
        return "books";
    }

    @Override
    public String getTitles() {
        return Formatter.format("%-30s%-30s%-30s\n", "Name", "Author", "Year");
    }
}

package com.twu.biblioteca;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;


public class BibliotecaApp {

    Printer printer;
    InputStream inputStream;

    BibliotecaApp(Printer printer, InputStream inputStream) {
        this.printer = printer;
        this.inputStream = inputStream;
    }

    public static void main(String[] args) throws Exception {
        Book[] books = {
                new Book("Harry Potter", "Vivek", "1992", false),
                new Book("Four  States", "Dhanesh", "1991", false)
        };
        Movie[] movies = {
                new Movie("Holiday", "2014", "Subhash", "8", false),
                new Movie("Shehenshah", "1992", "Sajid", "9", false)
        };
        User[] users = {
                new User("777-4445", "TW12", "Vivek", "vivekpatil2092@gmail.com", "9096904102", new ArrayList<Item>(10)),
                new User("654-8866", "Apple", "Chandan", "chandan00104@gmail.com", "9420473943", new ArrayList<Item>(10))
        };
        User currentUser = null;

        BookLibrary bookCollection = new BookLibrary(books);
        MovieLibrary movieCollection = new MovieLibrary(movies);
        UserCollection userCollection = new UserCollection(users);
        LibraryManager libraryManager = new LibraryManager(bookCollection, movieCollection, userCollection);
        new BibliotecaApp(new Printer(System.out), System.in).run(libraryManager, currentUser);
    }

    public void run(LibraryManager libraryManager, User currentUser) throws Exception {
        Scanner scanner = new Scanner(inputStream);
        scanner.useDelimiter("\n");
        printer.displayOutput("Welcome to Biblioteca\n");
        Menu.printMenu(printer, currentUser);
        while (scanner.hasNext()) {
            String option = scanner.next();
            Menu menu = Menu.find(option);
            ViewState viewState = menu.execute(scanner, currentUser, libraryManager, printer);
            if (!viewState.continueRunning) {
                break;
            }
            currentUser = viewState.currentUser;
            Menu.printMenu(printer, currentUser);
        }
    }
}

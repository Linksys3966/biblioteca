package com.twu.biblioteca;


import java.util.Scanner;

public class LibraryManager {
    BookLibrary bookLibrary;
    MovieLibrary movieCollection;
    UserCollection userCollection;

    LibraryManager(BookLibrary bookLibrary, MovieLibrary movieCollection, UserCollection userCollection) {
        this.bookLibrary = bookLibrary;
        this.movieCollection = movieCollection;
        this.userCollection = userCollection;
    }

    public ViewState checkOutBook(Scanner scanner, Printer printer, User currentUser) {
        printer.displayOutput("Please enter the name of the book to be checked out");
        String bookName = scanner.next();
        if (bookLibrary.checkOut(bookName, currentUser))
            printer.displayOutput("Thank You..Enjoy The Book");

        else
            printer.displayOutput("Sorry the Book is not Available");
        return new ViewState(currentUser, true);
    }

    public ViewState returnBook(Scanner scanner, Printer printer, User currentUser) {
        printer.displayOutput("Enter the Name Of the Book U want to return");
        String bookName = scanner.next();
        if (bookLibrary.checkIn(bookName, currentUser))
            printer.displayOutput("Thank You For Returning The Book");
        else
            printer.displayOutput("Sorry You are returning the wrong book");
        return new ViewState(currentUser, true);
    }

    public ViewState checkOutMovie(Scanner scanner, Printer printer, User currentUser) {
        printer.displayOutput("Please enter the name of the Movie to be checked out");
        String movieName = scanner.next();
        if (movieCollection.checkOut(movieName, currentUser))
            printer.displayOutput("Thank You..Enjoy The Movie");
        else
            printer.displayOutput("Sorry the Movie is not Available");
        return new ViewState(currentUser, true);
    }

    public ViewState returnMovie(Scanner scanner, Printer printer, User currentUser) {
        printer.displayOutput("Enter the Name Of the Movie U want to return");
        String movieName = scanner.next();
        if (movieCollection.checkIn(movieName, currentUser))
            printer.displayOutput("Thank You For Returning The Movie");
        else
            printer.displayOutput("Sorry You are returning the wrong Movie");
        return new ViewState(currentUser, true);
    }

    User login(Scanner scanner, Printer printer, UserCollection userCollection, User currentUser) {
        if (currentUser != null) {
            return currentUser;
        }
        printer.displayOutput("Enter Library Number");
        String libraryNo = scanner.next();
        printer.displayOutput("Enter Password");
        String password = scanner.next();
        User foundUser = userCollection.matches(libraryNo, password);
        if (foundUser == null) {
            printer.displayOutput("Invalid Login");
        }
        return foundUser;
    }


}

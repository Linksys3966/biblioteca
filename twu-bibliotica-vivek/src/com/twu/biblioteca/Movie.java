package com.twu.biblioteca;

public class Movie extends Item {
    private static final String FORMAT = "%-30s%-30s%-30s\n";
    public String name;
    public String year;
    public boolean checkedOut;
    String director;
    String movierating;

    public Movie(String name, String year, String director, String movierating, boolean checkedOut) {
        super(name, checkedOut);
        this.name = name;
        this.year = year;
        this.director = director;
        this.movierating = movierating;
        this.checkedOut = checkedOut;
    }

    @Override
    public String[] getFields() {
        return new String[]{name, director, year, movierating};
    }
}

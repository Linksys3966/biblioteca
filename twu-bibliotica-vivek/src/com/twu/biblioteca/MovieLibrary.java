package com.twu.biblioteca;

import java.util.Arrays;
import java.util.List;

public class MovieLibrary extends Library {
    List<Movie> movies;


    MovieLibrary(Movie... movies) {
        super(movies);
        this.movies = Arrays.asList(movies);
    }

    @Override
    protected String getItemName() {
        return "movies";
    }

    @Override
    public String getTitles() {
        return Formatter.format("%-30s%-30s%-30s\n", "NAME", "DIRECTOR", "YEAR", "RATING");
    }
}

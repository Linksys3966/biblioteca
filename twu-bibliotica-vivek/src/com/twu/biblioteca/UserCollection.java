package com.twu.biblioteca;

import java.util.Arrays;
import java.util.List;

public class UserCollection {

    private List<User> users;

    UserCollection(User... users) {
        this.users = Arrays.asList(users);
    }

    public User matches(String libraryNumber, String password) {
        for (User user : users) {
            if (user.matches(libraryNumber, password)) {
                return user;
            }
        }
        return null;
    }
}
